package com.moviesapi.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.moviesapi.R
import com.moviesapi.config.Config
import com.moviesapi.databinding.ItemMovieListBinding
import com.moviesapi.model.MovieResult
import com.moviesapi.model.MovieResultWrapper

class MovieListAdapter constructor(
    private val list: MovieResultWrapper,
    private val listener: ClickListener,
) : RecyclerView.Adapter<MovieListAdapter.ViewHolder>() {

    private lateinit var binding: ItemMovieListBinding
    private var listCopy: MutableList<MovieResult> = ArrayList(list.movieResult)

    inner class ViewHolder(view: ItemMovieListBinding) : RecyclerView.ViewHolder(view.root) {
        var name = view.movieName
        var image = view.imageLeft
        val rating = view.rating
        var parentLayout = view.constraintLayout
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        binding = DataBindingUtil.inflate(
            layoutInflater,
            R.layout.item_movie_list,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val result = listCopy[position]
        holder.name.text = result.name

        val rating = String.format(
            holder.rating.context.getString(R.string.label_rating),
            result.rating.toString()
        )
        holder.rating.text = rating
        holder.parentLayout.setOnClickListener {
            listener.onItemClicked(result)
        }
        Glide.with(holder.image)
            .load(Config.imgBaseUrl.plus(result.imageUrl))
            .centerCrop()
            .placeholder(R.drawable.ic_launcher_foreground)
            .error(R.drawable.ic_launcher_foreground)
            .circleCrop()
            .fallback(R.drawable.ic_launcher_foreground)
            .into(holder.image)
    }

    override fun getItemCount(): Int {
        return list.movieResult.size
    }

    fun updateMovieList(result: List<MovieResult>) {
        listCopy.clear()
        listCopy.addAll(result)
        this.notifyDataSetChanged()
    }

    interface ClickListener {
        fun onItemClicked(
            list: MovieResult
        )
    }
}