package com.moviesapi.config

object Config {
    const val movieApiEndPoint = "/3/tv/top_rated?api_key="
    const val imgBaseUrl = "https://image.tmdb.org/t/p/w500/"
    const val apiKey = "743cf0edb16da03747319d80751ff60a"
}