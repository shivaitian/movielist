package com.moviesapi.di

import android.content.Context
import com.moviesapi.movielist.MovieListUseCase
import com.moviesapi.service.VolleyService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)

class ApplicationModule {

    @Provides
    @Singleton
    fun providesNetworkRequestUseCase(
        context: Context,
        volleyService: VolleyService
    ): MovieListUseCase {
        return MovieListUseCase(context, volleyService)
    }

    @Provides
    @Singleton
    fun providesVolleyService(): VolleyService {
        return VolleyService
    }

    @Provides
    fun providesContext(@ApplicationContext context: Context): Context {
        return context
    }
}