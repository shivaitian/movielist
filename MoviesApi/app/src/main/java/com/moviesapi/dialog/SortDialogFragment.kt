package com.moviesapi.dialog

import android.app.Dialog
import android.graphics.Point
import android.os.Bundle
import android.view.*
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.fragment.app.DialogFragment
import com.moviesapi.R

class SortDialogFragment : DialogFragment(), View.OnClickListener {

    interface DialogUiCallback {
        fun updateSortResult(sortWith: CharSequence)
    }

    var radioGroup: RadioGroup? = null
    lateinit var radioButton: RadioButton
    private lateinit var button: TextView
    lateinit var mDialog: Dialog

    private lateinit var callback: DialogUiCallback
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.sort_dialog_fragment, container)
    }

    override fun onViewCreated(view: View, @Nullable savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // Fetch arguments from bundle and set title
        val title = arguments?.getString("title", "no value")
        mDialog.setTitle(title)
        mDialog.window!!.setSoftInputMode(
            WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE
        )
        radioGroup = view.findViewById(R.id.radioSortList)
        button = view.findViewById(R.id.clearText)
        button.setOnClickListener(this)
        radioGroup?.setOnCheckedChangeListener { radioGroup, i ->
            val selectedOption: Int = radioGroup!!.checkedRadioButtonId
            // Assigning id of the checked radio button
            radioButton = view.findViewById(selectedOption)
            mDialog.dismiss()
            callback.updateSortResult(radioButton.text)
        }
    }

    override fun onResume() {
        val window: Window? = dialog!!.window
        val size = Point()
        val display: Display = window?.windowManager!!.defaultDisplay
        display.getSize(size)
        window.setLayout((size.x * 0.90).toInt(), WindowManager.LayoutParams.WRAP_CONTENT)
        window.setGravity(Gravity.CENTER)
        // Call super onResume after sizing
        super.onResume()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        mDialog = super.onCreateDialog(savedInstanceState)
        mDialog.setCancelable(false)
        mDialog.setCanceledOnTouchOutside(false)
        return mDialog
    }

    fun setUiCallback(callback: DialogUiCallback) {
        this.callback = callback
    }

    companion object {
        fun newInstance(title: String?): SortDialogFragment {
            val frag = SortDialogFragment()
            val args = Bundle()
            args.putString("title", title)
            frag.arguments = args
            frag.setStyle(STYLE_NORMAL, R.style.CustomDialog)
            return frag
        }
    }

    override fun onClick(view: View) {
        callback.updateSortResult("clear")
        mDialog.dismiss()
    }
}