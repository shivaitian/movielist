package com.moviesapi.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "movie_list")
data class MovieResult(

    @PrimaryKey()
    @ColumnInfo(name = "movieName")
    @SerializedName("name")
    val name: String,

    @ColumnInfo(name = "posterPath")
    @SerializedName("poster_path")
    val imageUrl: String? = null,

    @ColumnInfo(name = "rating")
    @SerializedName("vote_average")
    val rating: Float,

    @ColumnInfo(name = "releaseDate")
    @SerializedName("first_air_date")
    val releaseDate: String,

    @ColumnInfo(name = "overview")
    @SerializedName("overview")
    val overview: String
)