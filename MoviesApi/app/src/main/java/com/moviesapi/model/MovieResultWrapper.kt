package com.moviesapi.model

import com.google.gson.annotations.SerializedName
import java.util.*

data class MovieResultWrapper(
    @SerializedName("results")
    val movieResult: List<MovieResult> = Collections.emptyList()
)
