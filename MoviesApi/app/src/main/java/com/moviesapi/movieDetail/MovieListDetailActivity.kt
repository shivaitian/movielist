package com.moviesapi.movieDetail

import android.os.Bundle
import com.moviesapi.R
import com.moviesapi.base.MovieBaseActivity
import dagger.hilt.android.AndroidEntryPoint

class MovieListDetailActivity : MovieBaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.movie_detail_activity)
        initActionBar(findViewById(R.id.toolbar))
        title = (getString(R.string.title_movie_Detail))
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, MovieListDetailFragment.newInstance())
                .commitNow()
        }
    }

    override fun getAppliedTheme(): Int {
        return R.style.ThemeOverlay_AppCompat_Light
    }
}