package com.moviesapi.movieDetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.google.gson.Gson
import com.moviesapi.R
import com.moviesapi.config.Config
import com.moviesapi.databinding.FragmentMovieDetailBinding
import com.moviesapi.model.MovieResult
import com.moviesapi.movielist.MovieListFragment.Companion.MOVIE_DETAIL

class MovieListDetailFragment : Fragment() {
    private var binding: FragmentMovieDetailBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_movie_detail,
            container,
            false
        )

        val extras = requireActivity().intent.extras
        if (extras == null || !extras.containsKey(MOVIE_DETAIL)) {
            throw IllegalArgumentException("You need to pass movie details params through intent")
        }
        val movieListDetail = extras.getString(MOVIE_DETAIL, "")
        setDetails(movieListDetail)
        return binding?.root!!
    }

    private fun setDetails(movieResult: String) {
        val result = getConvertedObjectFromJson(movieResult)
        Glide.with(binding!!.imgDetailImage)
            .load(Config.imgBaseUrl.plus(result.imageUrl))
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .centerCrop()
            .placeholder(R.drawable.ic_launcher_foreground)
            .error(R.drawable.ic_launcher_foreground)
            .fallback(R.drawable.ic_launcher_foreground)
            .into(binding!!.imgDetailImage)

        binding?.txtOverview?.text = result.overview
    }

    private fun getConvertedObjectFromJson(personalityDataWrapperJson: String?): MovieResult {
        return Gson().fromJson(personalityDataWrapperJson, MovieResult::class.java)
    }

    companion object {
        fun newInstance() =
            MovieListDetailFragment()
    }
}