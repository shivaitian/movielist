package com.moviesapi.movielist

import android.os.Bundle
import com.moviesapi.R
import com.moviesapi.base.MovieBaseActivity
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MovieListActivity : MovieBaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initActionBar(findViewById(R.id.toolbar))
        title = (getString(R.string.title_movie_list))
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, MovieListFragment.newInstance())
                .commitNow()
        }
    }

    override fun getAppliedTheme(): Int {
        return R.style.ThemeOverlay_AppCompat_Light
    }
}