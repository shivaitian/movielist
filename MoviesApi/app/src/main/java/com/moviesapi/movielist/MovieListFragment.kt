package com.moviesapi.movielist

import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.moviesapi.R
import com.moviesapi.adapter.MovieListAdapter
import com.moviesapi.databinding.FragmentMovieListBinding
import com.moviesapi.dialog.SortDialogFragment
import com.moviesapi.logs.Logger
import com.moviesapi.model.MovieResult
import com.moviesapi.model.MovieResultWrapper
import com.moviesapi.movieDetail.MovieListDetailActivity
import com.moviesapi.room.MovieListStoreRoomDatabase
import com.moviesapi.service.VolleyService
import com.moviesapi.util.Util
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
open class MovieListFragment : Fragment(), CoroutineScope by MainScope(),
    MovieListAdapter.ClickListener, SortDialogFragment.DialogUiCallback,
    MovieListViewModel.UiCallback {
    private lateinit var adapter: MovieListAdapter
    private lateinit var database: MovieListStoreRoomDatabase
    private var menuItem: MenuItem? = null

    @Inject
    lateinit var movieListUseCase: MovieListUseCase
    private var binding: FragmentMovieListBinding? = null

    @Inject
    lateinit var volleyService: VolleyService
    lateinit var viewModel: MovieListViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_movie_list,
            container,
            false
        )
        database = MovieListStoreRoomDatabase.getDatabase(requireActivity().applicationContext)

        val viewModelFactory =
            MovieListViewModelFactory(movieListUseCase, database)
        viewModel = ViewModelProvider(
            requireActivity(),
            viewModelFactory
        ).get(MovieListViewModel::class.java)
        binding?.viewModel = viewModel

        if (Util.isNetworkAvailable(requireContext())) {
            binding?.txtNoInternet?.visibility = View.GONE
            binding?.searchProgress?.visibility = View.VISIBLE

            this.launch {
                Dispatchers.IO
                viewModel.loadMovieData()
            }
        } else {
            binding?.txtNoInternet?.visibility = View.VISIBLE
            binding?.searchProgress?.visibility = View.GONE
        }

        handleObserver()

        setHasOptionsMenu(true)

        return binding?.root!!
    }

    private fun handleObserver() {
        viewModel.movieData.observe(viewLifecycleOwner, {
            refreshUi(it)
        })
    }

    private fun refreshUi(movieResult: MovieResultWrapper) {
        insertToDatabase(movieResult)
        adapter = MovieListAdapter(movieResult, this)
        val layoutManager = LinearLayoutManager(context)
        binding?.recyclerMovieList?.layoutManager = layoutManager
        binding?.recyclerMovieList?.itemAnimator = DefaultItemAnimator()
        binding?.recyclerMovieList?.adapter = adapter
        menuItem?.isVisible = true
    }

    private fun insertToDatabase(movieResultWrapper: MovieResultWrapper) {
        this.launch {
            Dispatchers.IO
            viewModel.insertToDatabase(movieResultWrapper.movieResult)
        }
    }

    override fun onItemClicked(movieResult: MovieResult) {
        Logger.i("item click", "clicked")
        val intent = Intent(requireContext(), MovieListDetailActivity::class.java)
        intent.putExtra(MOVIE_DETAIL, Gson().toJson(movieResult))
        startActivity(intent)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.personality_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.menu_sort) {
            showEditDialog()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        menuItem = menu.findItem(R.id.menu_sort)
        menuItem?.isVisible = false
    }

    private fun showEditDialog() {
        val fm = requireActivity().supportFragmentManager
        val sortFragment = SortDialogFragment.newInstance("Sort list")
        sortFragment.setUiCallback(this)
        sortFragment.show(fm, "DD")
    }

    override fun updateSortResult(sortWith: CharSequence) {
        this.launch {
            Dispatchers.IO
            viewModel.sort(requireContext(), this@MovieListFragment, sortWith)
        }
    }

    override fun onSortCompleted(result: MutableList<MovieResult>) {
        if (!result.isNullOrEmpty()) {
            adapter.updateMovieList(result)
        }
    }

    companion object {
        fun newInstance() =
            MovieListFragment()

        const val MOVIE_DETAIL = "movieDetail"

    }
}