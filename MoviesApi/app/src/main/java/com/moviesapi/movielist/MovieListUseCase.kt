package com.moviesapi.movielist

import android.content.Context
import com.moviesapi.BuildConfig
import com.moviesapi.config.Config
import com.moviesapi.logs.Logger
import com.moviesapi.model.MovieResultWrapper
import com.moviesapi.service.GsonRequest
import com.moviesapi.service.VolleyService
import kotlinx.coroutines.suspendCancellableCoroutine
import javax.inject.Inject
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException

class MovieListUseCase @Inject constructor(
    private val context: Context,
    private val volleyService: VolleyService
) {
    private val url = BuildConfig.SERVER_URL.plus(Config.movieApiEndPoint).plus(Config.apiKey)

    suspend fun loadMovieData(): MovieResultWrapper {
        return suspendCancellableCoroutine { result ->
            val request: GsonRequest<MovieResultWrapper> = GsonRequest(
                url,
                MovieResultWrapper::class.java,
                {
                    if (result.isActive) {
                        result.resume(it)
                    }
                },
                {
                    Logger.e("Error", result.toString())
                    result.resumeWithException(VolleyException(it))
                }
            )

            result.invokeOnCancellation {
                result.cancel()
            }
            volleyService.getRequestQueue(context).add(request)
        }
    }
}