package com.moviesapi.movielist

import android.content.Context
import androidx.annotation.VisibleForTesting
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.moviesapi.R
import com.moviesapi.logs.Logger
import com.moviesapi.model.MovieResult
import com.moviesapi.model.MovieResultWrapper
import com.moviesapi.room.MovieListStoreRoomDatabase
import javax.inject.Inject

open class MovieListViewModel @Inject constructor(
    private val movieListUseCase: MovieListUseCase,
    private val database: MovieListStoreRoomDatabase
) :
    ViewModel() {
    interface UiCallback {
        fun onSortCompleted(result: MutableList<MovieResult>)
    }

    lateinit var result: MutableList<MovieResult>

    var movieData: MutableLiveData<MovieResultWrapper> = MutableLiveData()
    var progressBarStatus = ObservableBoolean(false)

    suspend fun loadMovieData() {
        progressBarStatus.set(true)
        handleResponse(movieListUseCase.loadMovieData())
    }

    fun handleResponse(result: MovieResultWrapper) {
        Logger.d("response", result.toString())
        progressBarStatus.set(false)
        movieData.postValue(result)
    }

    suspend fun sort(
        context: Context,
        uiCallback: UiCallback,
        sortWith: CharSequence,
    ) {
        when (sortWith) {
            context.getString(R.string.label_sort_az) -> {
                result = database.movieListStoreDao().getPersonalityListSortAsc()
            }
            context.getString(R.string.label_sort_za) -> {
                result = database.movieListStoreDao().getPersonalityListSortDesc()
            }
            context.getString(R.string.label_sort_release_date) -> {
                result = database.movieListStoreDao().getPersonalityListSortByReleaseDate()
            }
            context.getString(R.string.label_sort_rating) -> {
                result = database.movieListStoreDao().getPersonalityListSortByRating()
            }
            else -> {
                result = database.movieListStoreDao().getPersonalityListDefaultOrder()
            }
        }
        uiCallback.onSortCompleted(result)
    }

    @VisibleForTesting
    fun getListInitialized(): MutableList<MovieResult> {
        result = ArrayList()
        return result
    }

    suspend fun insertToDatabase(movieResult: List<MovieResult>) {
        movieResult.forEach {
            database.movieListStoreDao().insert(
                MovieResult(
                    it.name,
                    it.imageUrl ?: "NA",
                    it.rating,
                    it.releaseDate,
                    it.overview
                )
            )
        }
    }
}