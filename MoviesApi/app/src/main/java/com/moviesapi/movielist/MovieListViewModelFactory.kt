package com.moviesapi.movielist

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.moviesapi.room.MovieListStoreRoomDatabase

class MovieListViewModelFactory(
    private val movieListUseCase: MovieListUseCase,
    private val database: MovieListStoreRoomDatabase
) :
    ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MovieListViewModel::class.java)) {
            return MovieListViewModel(movieListUseCase, database) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}