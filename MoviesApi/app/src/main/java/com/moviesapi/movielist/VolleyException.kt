package com.moviesapi.movielist

import com.android.volley.VolleyError

class VolleyException(error: VolleyError) : Exception()