package com.moviesapi.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = "movie_list")
class MovieListStore(
    @PrimaryKey()
    @ColumnInfo(name = "movieName")
    val movieName: String,
    @ColumnInfo(name = "releaseDate")
    val releaseDate: String,
    @ColumnInfo(name = "rating")
    val rating: String,
    @ColumnInfo(name = "posterPath")
    val posterPath: String,
    @ColumnInfo(name = "overview")
    val overview: String,
)