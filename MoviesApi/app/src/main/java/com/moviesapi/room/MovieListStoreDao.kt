package com.moviesapi.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.moviesapi.model.MovieResult

@Dao
interface MovieListStoreDao {

    @Query("SELECT * FROM movie_list")
    suspend fun getPersonalityListDefaultOrder(): MutableList<MovieResult>

    @Query("SELECT * FROM movie_list ORDER BY movieName ASC")
    suspend fun getPersonalityListSortAsc(): MutableList<MovieResult>

    @Query("SELECT * FROM movie_list ORDER BY movieName DESC")
    suspend fun getPersonalityListSortDesc(): MutableList<MovieResult>

    @Query("SELECT * FROM movie_list ORDER BY rating ASC")
    suspend fun getPersonalityListSortByRating(): MutableList<MovieResult>

    @Query("SELECT * FROM movie_list ORDER BY releaseDate ASC")
    suspend fun getPersonalityListSortByReleaseDate(): MutableList<MovieResult>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(movieListStore: MovieResult)

    @Query("DELETE FROM movie_list")
    suspend fun deleteAll()
}