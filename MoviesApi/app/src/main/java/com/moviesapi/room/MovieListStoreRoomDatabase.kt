package com.moviesapi.room

import android.annotation.SuppressLint
import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.moviesapi.model.MovieResult

@Database(entities = [MovieResult::class], version = 1, exportSchema = false)
abstract class MovieListStoreRoomDatabase : RoomDatabase() {

    abstract fun movieListStoreDao(): MovieListStoreDao

    companion object {
        @Volatile
        private var INSTANCE: MovieListStoreRoomDatabase? = null

        @SuppressLint("SyntheticAccessor")
        fun getDatabase(context: Context): MovieListStoreRoomDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    MovieListStoreRoomDatabase::class.java,
                    "movie_list_store_database"
                ).build()
                INSTANCE = instance
                instance
            }
        }
    }
}