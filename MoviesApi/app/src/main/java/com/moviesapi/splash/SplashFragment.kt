package com.moviesapi.splash

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatButton
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.moviesapi.R
import com.moviesapi.movielist.MovieListActivity
import com.personality.main.splash.SplashViewModel

class SplashFragment : Fragment(), View.OnClickListener {

    lateinit var button: AppCompatButton
    private fun navigate() {
        val intent = Intent(requireContext(), MovieListActivity::class.java)
        startActivity(intent)
        requireActivity().finish()
    }

    private lateinit var viewModel: SplashViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.splash_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        button = view.findViewById(R.id.btnLetsGo)
        button.setOnClickListener(this@SplashFragment)
        viewModel = ViewModelProvider(this).get(SplashViewModel::class.java)
    }

    override fun onClick(view: View?) {
        navigate()
    }

    companion object {
        fun newInstance() = SplashFragment()
    }

}