package com.moviesapi.adapter

import android.content.Context
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import com.moviesapi.model.MovieResult
import com.moviesapi.model.MovieResultWrapper
import com.nhaarman.mockitokotlin2.whenever
import dagger.hilt.android.qualifiers.ApplicationContext
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.Mock
import org.mockito.MockitoAnnotations.initMocks

class MovieListAdapterTest {
    lateinit var subject: MovieListAdapter

    @MockK
    lateinit var personalityDataWrapperMock: MovieResultWrapper

    @MockK
    lateinit var uiListener: MovieListAdapter.ClickListener

    @Mock
    lateinit var contextMock: Context


    @Before
    fun setUp() {
        initMocks(this)
        MockKAnnotations.init(this, relaxUnitFun = true)
        every { personalityDataWrapperMock.movieResult } returns listOf(
            MovieResult("aa", "", 22.3F, "20-8-2021", "test"),
            MovieResult("bb", "", 11.3F, "20-8-2021", "test")
        ).toMutableList()
        subject = MovieListAdapter(personalityDataWrapperMock, uiListener)
    }

    @Test
    fun `it should return proper count of items`() {

        val count = subject.itemCount

        Assert.assertEquals(2, count)
    }

    @Test
    fun itShouldNotifyWhenDataSetChanges() {
        whenever(contextMock.getString(anyInt())).thenReturn("Sort A-Z")
        //subject.updateMovieList("Sort A-Z")
    }
}