package com.moviesapi.movielist

import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.moviesapi.logs.Logger
import com.moviesapi.model.MovieResult
import com.moviesapi.model.MovieResultWrapper
import com.moviesapi.room.MovieListStoreDao
import com.moviesapi.room.MovieListStoreRoomDatabase
import com.nhaarman.mockitokotlin2.whenever
import com.personality.main.logs.LogInterface
import io.mockk.MockKAnnotations
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.Mock
import org.mockito.MockitoAnnotations.initMocks

class MovieListViewModelTest {
    @MockK
    lateinit var movieListUseCase: MovieListUseCase

    @MockK
    lateinit var movieResultWrapper: MovieResultWrapper

    @MockK
    lateinit var liveDataMock: MutableLiveData<MovieResultWrapper>

    @Mock
    lateinit var databaseMock: MovieListStoreRoomDatabase

    @Mock
    lateinit var contextMock: Context

    @Mock
    lateinit var movieListStoreDaoMock: MovieListStoreDao

    @MockK
    lateinit var uiCallback: MovieListViewModel.UiCallback

    @MockK
    lateinit var logger: LogInterface

    private lateinit var subject: MovieListViewModel

    @MockK
    private lateinit var sortWithMock: CharSequence

    @Before
    fun setUp() {
        initMocks(this)
        MockKAnnotations.init(this, relaxUnitFun = true)
        Logger.setLoggerInterface(logger)
        subject = MovieListViewModel(movieListUseCase, databaseMock)
    }

    @Test
    fun `it should return progressbar status as true when data is initially loaded`() {
        subject.movieData = liveDataMock

        Assert.assertFalse(subject.progressBarStatus.get())
    }

    @Test
    fun `it should return progressbar status as false when data is fully loaded`() {
        subject.movieData = liveDataMock
        subject.handleResponse(movieResultWrapper)

        Assert.assertFalse(subject.progressBarStatus.get())
    }

    @Test
    fun `it should sort data successfully`() = runBlocking {
        val list: MutableList<MovieResult> = ArrayList()
        list.add(MovieResult("AA", "na", 22.5F, "21-4-2020", "test"))
        whenever(contextMock.getString(anyInt())).thenReturn("Sort A-Z")
        whenever(databaseMock.movieListStoreDao()).thenReturn(movieListStoreDaoMock)
        whenever(movieListStoreDaoMock.getPersonalityListDefaultOrder()).thenReturn(list)
        sortWithMock = "Sort A-Z"
        subject.sort(contextMock, uiCallback, sortWithMock)

        // verify { uiCallback.onSortCompleted(any()) }
    }
}