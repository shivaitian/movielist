# README #

App Architecture

1. Used Jetpack MVVM for the assignment
2. Hilt is used as dependeny Injection
3. Coroutine for IO operations
4. Glide is utilized to fetch images from remote url
5. Romm database is used to persist the data in local storage
6. Mockito and power mock for unit test 
7. to fetch api data volley is used as networking library

Gradle

1. have created gradle script to record versions and flavour
2. On launching the app launches the splash screen with a logo
3. After the splash comes the Movie list screen gets called
4. We can sort the list by tapping the sort option from the top header
5. once list is loaded from api end point, it gets saved into local db
6. Pressing on the card from the movie list opens the detail page.


Note: attached is the sequence diagram for reference.


